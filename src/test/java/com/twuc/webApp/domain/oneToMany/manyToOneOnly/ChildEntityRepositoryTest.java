package com.twuc.webApp.domain.oneToMany.manyToOneOnly;

import com.twuc.webApp.domain.ClosureValue;
import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class ChildEntityRepositoryTest extends JpaTestBase {

    @Autowired
    private ChildEntityRepository childEntityRepository;

    @Autowired
    private ParentEntityRepository parentEntityRepository;

    @Test
    void should_save_parent_and_child_entity() {
        // TODO
        //
        // 请书写测试存储一对 one-to-many 的 child-parent 关系。并从 child 方面进行查询来证实存储已经
        // 成功。
        //
        // <--start-
        ClosureValue<Long> childId = new ClosureValue<>();
        flushAndClear(em -> {
            ParentEntity parentEntity = new ParentEntity("parent");
            ChildEntity childEntity = new ChildEntity("child");
            em.persist(parentEntity);
            em.persist(childEntity);
            childEntity.setParentEntity(parentEntity);
            childId.setValue(childEntity.getId());
        });

        run(em -> {
            ChildEntity childEntity = em.find(ChildEntity.class, childId.getValue());
            assertEquals("parent", childEntity.getParentEntity().getName());
        });
        // --end-->
    }

    @Test
    void should_remove_the_child_parent_relationship() {
        // TODO
        //
        // 请书写测试：
        //
        // Given 一对 one-to-many 的 child-parent 关系。
        // When 解除 child 和 parent 的关系
        // Then child 和 parent 仍然存在，但是 child 不再引用 parent。
        //
        // <--start-
        ClosureValue<Long> childId = new ClosureValue<>();
        ParentEntity parentEntity = new ParentEntity("parent");
        ChildEntity childEntity = new ChildEntity("child");
        flushAndClear(em -> {
            em.persist(parentEntity);
            em.persist(childEntity);
            childEntity.setParentEntity(parentEntity);
            childId.setValue(childEntity.getId());
        });
        flushAndClear(em -> {
            ChildEntity child = em.find(ChildEntity.class, childId.getValue());
            childEntity.setParentEntity(null);
        });

        run(em -> {
            ChildEntity child = em.find(ChildEntity.class, childId.getValue());
            assertNull(childEntity.getParentEntity());
            assertEquals("parent",parentEntity.getName());
            assertEquals("child",childEntity.getName());
        });
        // --end-->
    }

    @Test
    void should_remove_child_and_parent() {
        // TODO
        //
        // 请书写测试：
        //
        // Given 一对 one-to-many 的 child-parent 关系。
        // When 删除 child 和 parent。
        // Then child 和 parent 不再存在。
        //
        // <--start-
        ParentEntity parent = new ParentEntity("parent");
        ChildEntity child = new ChildEntity("child");
        flushAndClear(em->{
            em.persist(parent);
            em.persist(child);
            child.setParentEntity(parent);
        });
        flushAndClear(em->{
            ParentEntity parentEntity = em.find(ParentEntity.class, parent.getId());
            ChildEntity childEntity = em.find(ChildEntity.class, child.getId());
            em.remove(parentEntity);
            em.remove(childEntity);
        });
        flushAndClear(em->{
            ParentEntity parentEntity = em.find(ParentEntity.class, parent.getId());
            ChildEntity childEntity = em.find(ChildEntity.class, child.getId());
            assertNull(parentEntity);
            assertNull(childEntity);
        });
        // --end-->
    }
}